			
			<!--This section is controlled via the Navigation.php-->
			<div class="navbar navbar-inverse navbar-fixed-top">
			      <div class="navbar-inner">
			        <div class="container">
			          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			          </a>
			          <a class="brand" href="#">Scratchpad</a>
			          <div class="nav-collapse collapse">
			            <ul class="nav">
			              <li class="active"><a href="#">Home</a></li>
			              <li><a href="#Email">Email</a></li>
			              <li><a href="#Email with validation">Email with validation</a></li>
			              <li><a href="#CRUD managment">CRUD Managment</a></li>
			            </ul>
			          </div><!--/.nav-collapse -->
			        </div>
			      </div>
			    </div>
			<!--This ends the navigation section-->