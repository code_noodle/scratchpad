<?php $attributes = array('class' => '.form-horizontal', 'id' => 'menu');//array to be passed to form_open() ?>

<div class="span3">

	<div id="SearchForm">
		<!--begins the Search form, information entered by the user here is passed to the ShowRecords method in the 
			Database_Practise controller-->
		<?php
		echo form_open('database_practice/showrecords', $attributes);
		?>

		<legend>
			Search The Posts Database
		</legend>
		<label>Search By Author:</label>
		<div class="input-append">
			<input type="text" class="input-large search-query" placeholder="Search for a post by Author" name="SearchAuthor">

		</div>
		<label>Search By Date:</label>
		<div class="input-append">
			<input type="text" class="input-large search-query" placeholder="Search for a post by Date" name="SearchDate">

		</div>
		<label>Search By Title:</label>
		<div class="input-append">
			<input type="text" class="input-large search-query" placeholder="Search for a post by Title" name="SearchTitle">

		</div>
		<label>Search By Content:</label>
		<div class="input-append">
			<input type="text" class="input-large search-query" placeholder="Search for a post by Content" name="SearchContent">

		</div>
		<div>
			<label class="radio">
				<input type="radio" name="ResultsNumber" id="GetAll" value="GetAll" checked>
				Get All Results. </label>

			<label class="radio">
				<input type="radio" name="ResultsNumber" id="GetNum" value="GetNum">
				Select a set number of results. </label>
			<input name="ResultsRequired" class="input-medium search-query" type="text" placeholder="How Many Results?">
		</div>

		<p>
			<button id="QueryButton" type="submit" class="btn btn-large btn-inverse btn-block">
				Fetch Posts!
			</button>
		</p>

		<?php echo form_close(); ?>

	</div>
	<br />
	<div id="ManageDatabaseForms">
		<!--These buttons enable the user to create new posts, edit selected posts and delete selected posts-->
		<legend>Manage the Posts Database</legend>
		<?php
		echo form_open('Database_Practice/CreatePost', $attributes);
		?>
		<p>
			<button id="QueryButton" type="submit" class="btn btn-large btn-block">
				Create New Post
			</button>
		</p>
		<?php echo form_close(); ?>
		
		<?php
		echo form_open('Database_Practice/EditPost', $attributes);
		?>
		<p>
			<button id="QueryButton" type="submit" class="btn btn-large btn-block">
				Edit Selected Post
			</button>
		</p>
		<?php echo form_close(); ?>
		
		<?php
		echo form_open('Database_Practice/DeletePost', $attributes);
		?>
		<p>
			<button id="QueryButton" type="submit" class="btn btn-large btn-danger btn-block">
				Delete Selected Posts
			</button>
		</p>
		<?php echo form_close(); ?>
	</div>

</div>
</div>
</div>