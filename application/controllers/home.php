<?php

/**
 * This is the main index controller for the site. 
 */
class home extends CI_Controller {
	
	function index() {
				
		$HeaderData = array('Title' => 'Welcome to Scratchpad' );
		//Information in the above array is passed to the header.php file.
		//Must include a Title value.
		
		//Loads the view
		$this->load->view('Header_View.php', $HeaderData);
		$this->load->view('Navigation_View.php');
		$this->load->view('Body_Home_View.php');
		$this->load->view('Footer_View.php');
	}
	
}
