<?php

/**
 * Database Access
 */
class Database_Practice extends CI_Controller {
	
	
	
	//this loads a blank page ready to accept database query.
	function index() {
		
		$HeaderData = array('Title' => 'Database Access' );
		//Information in the above array is passed to the header.php file.
		//Must include a Title value.
		
		
		//Loads the view
		$this->load->view('Header_View', $HeaderData);
		$this->load->view('Navigation_View');
		$this->load->view('Body_Database_Practice_No_Results_View');
		$this->load->view('Sidebar_Query_Form_View');
		$this->load->view('Footer_View');
	}
	
	
	
	function ShowRecords()//access the database using the getAll or getNum functions in the Database_Practice_Model
	{
		
		$HeaderData = array('Title' => 'Database Access' );
		//captures the information from the search form.
		$Author = $_POST['SearchAuthor'];
		$Date = $_POST['SearchDate'];
		$Title = $_POST['SearchTitle'];
		$Content =  $_POST['SearchContent'];
		
		if ($_POST['ResultsNumber'] == "GetAll"){
			$numberOfResults = "All";
		} else {
			$numberOfResults = $_POST['ResultsRequired'];
		}
		
		//puts the information into an array to be used by the model. 
		$ModelData = array( 'AuthorFirstName' => $Author,
							'DateOfPost' 	 => $Date,
							'PostTitle'  => $Title,
							'PostBody'=> $Content);
		
		//loads the model, runs GetResults with the values from $ModelData, then captures the result in $ReturnedData
		$this->load->model('Database_Practice_Model');
		$query['row'] = $this->Database_Practice_Model->GetResults($ModelData, $numberOfResults);
		
								
		$this->load->view('Header_View.php', $HeaderData);
		$this->load->view('Navigation_View.php');
		$this->load->view('Body_Database_Practice_view.php', $query);
		$this->load->view('Sidebar_Query_Form_View');
		$this->load->view('Footer_View');
	 	
	 	
	}
}
